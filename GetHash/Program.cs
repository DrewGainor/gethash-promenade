﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace GetHash
{
    class Program
    {
        //static string fileToCheck = @"D:\TeleflexSystemUpdates.exe";
        static string fileToCheck = @"H:\TeleflexSystemUpdates.exe";
        //Drew's signtool location
        //const string strSignToolPath = @"C:\Program Files (x86)\Windows Kits\10\bin\10.0.18362.0\x64\";
        //Duo Unit and Jame's signtool location
        static string strSignToolPath = @"C:\Program Files (x86)\Windows Kits\10\bin\10.0.19041.0\x86\";

        static void Main(string[] args)
        {
            //Retreive certificate hash from signed file
            string signedHash = GetSignedHash();
            //retreive certificate thumbprint from Current User Trusted Root Certification Authorities store
            string trustedRootHash = GetCertificateFromCurrentUserStore();

            //Do the hash and the thumbprint match
            if (signedHash == trustedRootHash)
                Console.WriteLine("They Match");
            else
                Console.WriteLine("They don't match");

            Console.WriteLine("Any key to continue.");
            Console.ReadKey();
        }

        public static string GetSignedHash()
        {
            //does signed file exist
            if (File.Exists(fileToCheck))
            {
                string hash = "";
                // Launch signtool.
                Process process = new Process();
                process.StartInfo.FileName = Path.Combine(strSignToolPath, "signtool.exe");

                process.StartInfo.Arguments = "verify /pa /v " + (char)34 + fileToCheck + (char)34;
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;
                process.Start();

                // Read the output (or the error).
                string output = process.StandardOutput.ReadToEnd();
                string err = process.StandardError.ReadToEnd();
                process.WaitForExit();

                //Place output into array while removing leading whitespace and all carriage returns
                string[] stringSeparators = new string[] { "\r\n" };
                stringSeparators = new string[] { "\r" };
                string[] lines = output.Split(stringSeparators, StringSplitOptions.None);

                //parse array for Issued To
                int index = Array.FindIndex(lines, s => s.Contains("Issued to:"));
                string name = lines[index].TrimStart();
                name = name.Replace("Issued to: ", "");
                //Console.WriteLine("Issued to:  {0}", name);

                //parse array for Expires
                index = Array.FindIndex(lines, s => s.Contains("Expires:"));
                string expires = lines[index].TrimStart();
                expires = expires.Replace("Expires:    ", "");
                //Console.WriteLine("Expires:    {0}", expires);

                //parse array for hash
                index = Array.FindIndex(lines, s => s.Contains("SHA1 hash:"));
                hash = lines[index].TrimStart();
                hash = hash.Replace("SHA1 hash: ", "");
                Console.WriteLine("SHA1 hash:  {0}", hash);
                return hash;
            }
            else
            {
                string message = "Signtool Error: " + fileToCheck + " not found.";
                Console.WriteLine(message);

                Console.WriteLine("NotFound"); // We never get here; just preventing compile error.
            }
            return "error";
        }

        public static string GetCertificateFromCurrentUserStore()
        {
            //Get a list of all certificates in the Current User Trusted Root Certification Authorities store
            var localMachineStore = new X509Store(StoreName.Root, StoreLocation.CurrentUser);
            localMachineStore.Open(OpenFlags.ReadOnly);
            var certificates = localMachineStore.Certificates;
            localMachineStore.Close();
            //Loop through list of certificates to find the Duo.Teleflex Thumbprint
            X509Certificate2 certificate = null;
            foreach (var cert in certificates.Cast<X509Certificate2>().Where(cert => cert.Subject.Contains("Duo.Teleflex")))
            {
                certificate = cert;
            }
            Console.WriteLine("Thumbprint: {0}", certificate.Thumbprint);
            return certificate.Thumbprint;
        }
    }
}

